def test_os(host):
    assert host.system_info.type == "linux"
    assert host.system_info.distribution == "alpine"

def test_nginx(host):
    nginx = host.package("nginx")
    assert nginx.is_installed
    assert nginx.version.startswith("1.16.1")